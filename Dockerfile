FROM openjdk:17-oracle
MAINTAINER baeldung.com
COPY backend-0.0.1-SNAPSHOT.jar backend-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/backend-0.0.1-SNAPSHOT.jar"]
