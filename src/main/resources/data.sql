DELETE FROM item;
INSERT INTO item (id, name, price) VALUES (1, 'Shoes', 60.00);
INSERT INTO item (id, name, price) VALUES (2, 'T-shirt', 20.00);
INSERT INTO item (id, name, price) VALUES (3, 'Car', 40.00);
INSERT INTO item (id, name, price) VALUES (4, 'Bike', 35.00);
INSERT INTO item (id, name, price) VALUES (5, 'Board', 15.00);