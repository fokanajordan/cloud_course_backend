package fr.efrei.backend.service;

import fr.efrei.backend.domain.Item;
import fr.efrei.backend.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {
    @Autowired
    ItemRepository itemRepository;
    public List<Item> findAll(){return itemRepository.findAll();}
}
